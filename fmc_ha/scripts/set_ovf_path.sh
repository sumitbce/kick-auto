#!/bin/bash

export SCRIPT_HOME='/home/admin/sumpande/kick-auto/fmc_ha/'
export CONFIG_FILE='/home/admin/sumpande/ws/basic_ha_sanity/params'

# sudo apt-get install python3-yaml

python3 $SCRIPT_HOME/scripts/find_latest_ovf.py -t $SCRIPT_HOME/testbeds/test.yaml -b dev | while read line ; do
    if [[ $(echo $line | grep  'Management') ]]; then
        echo "FMC_OVF_PATH=${line}" >> $CONFIG_FILE
    else
        echo "FTD_OVF_PATH=${line}" >> $CONFIG_FILE
    fi
done

grep 'Firepower_Management_Center_Virtual_VMware-VI' $CONFIG_FILE | awk -F '/|-' '{print "Branch=" $5 "\nVersion=" $6 "\nBuild=" $7}' >>  $CONFIG_FILE
cat $CONFIG_FILE

