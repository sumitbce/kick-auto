#!/usr/bin/python3
import re
import os
import sys
import yaml
import argparse
import urllib.request
from urllib.error import URLError

BUILDS = []
data = {}
UPG_URL = None
IMAGE_PATH = []

# Start with highest build and check if the file is present
# If yes, return the build number, otherwise check for the second highest build number
# Repeat this till CURRENT_BUILD

def get_latest_build(sorted_build):
    for build in sorted_build:
        IMAGE_PATH.clear()
        # Recreate path
        build_path = data[branch]['version'] + '-' + str(build)

        image_found = 0
        search_pattern = data[branch][image_type]['image_search_pattern']
        for ipath in data[branch][image_type]['img_path']:
            img_url = os.path.join(data[branch]['url'], build_path, ipath)

            if args.debug:
                print("\n IMG_URL: %s" % img_url)

            res = urlopen(img_url)
            if res is None:
                continue

            if args.debug:
                print(res)

            for image in search_pattern:
                for line in res:
                    image_search = re.search(image, str(line))
                    if image_search:
                        image_found = image_found + 1
                        IMAGE_PATH.append(os.path.join(img_url,  image_search.group(0)))
                        if args.debug:
                            print("Image {%s} found in {%s} found." % (image, line))

                        break

        if len(search_pattern) == image_found:
            if args.debug:
                print("All image found in build {}".format(build))
            return build
        else:
            if args.debug:
                print("All images not found. Checking earlier build")

    print("No build with all images matches the criteria")
    sys.exit(1)


def urlopen(url):
    req = urllib.request.Request(url)
    try:
        response = urllib.request.urlopen(req)
    except URLError as e:
        if hasattr(e, 'reason'):
            if args.debug:
                print('We failed to reach a server.')
                print('Reason: ', e.reason)
            return None
        elif hasattr(e, 'code'):
            if args.debug:
                print('The server couldn\'t fulfill the request.')
                print('Error code: ', e.code)
                return None

    return response.read().splitlines()


def load_yaml(file):
    with open(file, 'r') as stream:
        try:
            data = yaml.load(stream)
            if args.debug:
                print(data)
        except yaml.YAMLError as exc:
            print(exc)
    return data

def find_build():

    res = urlopen(data[branch]['url'])

    for line in res:
        line = line.decode("utf-8")
        if data[branch]['version'] in line:
            build_search = re.search(r'%s-(\d+)/' % data[branch]['version'], line)
            if build_search:
                BUILDS.append(int(build_search.group(1)))


    if args.debug:
        print("Build : %s" % BUILDS + "\n")

    if data[branch]['build_upper_limit']:
        filtered_builds = [build for build in BUILDS
                if data[branch]['current_build'] < build <= data[branch]['build_upper_limit']]

    else:
        filtered_builds = [build for build in BUILDS
                           if build > data[branch]['current_build']]

    if args.debug:
        print("Filtered Build in ascending order: %s" % filtered_builds)

    # Sort will print the highest build
    sorted_builds = (sorted(filtered_builds, key=int, reverse=True))

    if args.debug:
        print("Sorted Build in ascending order: %s" % sorted_builds)
    return sorted_builds


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Find the latest build')
    parser.add_argument('-t', action='store', dest='testbed', help='Custom config file')
    parser.add_argument('-b', action='store', dest='branch', default='dev', help='Specify branch (dev|rel)')
    parser.add_argument('-i', action='store', dest='image_type', default='upgrade',
                        help='Specify type of image (upgrade|deploy)')
    parser.add_argument('-d', action='store_true', default=False, dest='debug', help='Flag to enable debug mode')
    args = parser.parse_args()

    data = (load_yaml(args.testbed))['testbed']
    branch = args.branch
    image_type = 'deploy' #args.image_type, fmc_deploy
    sorted_build = find_build()
    latest_build = get_latest_build(sorted_build)

    if args.debug:
        print("Latest found build with all image : %s" % latest_build)

    # For upgrade build no is important whereas for deployment full ovf path
    if image_type == 'upgrade':
        print(latest_build)
    else:
        for path in IMAGE_PATH:
            print(path)
